<?php include_once('../inc_pages.php'); ?>
<?php
  /* 
   * Paging
   */
   
  // actualiza estado dos registos selecionados
  if(isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    //$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
  
    // actualiza estado dos registos selecionados
    $opcao = $_REQUEST["customActionName"];
    $array_ids = $_REQUEST["id"];
    $lista = "";
    foreach($array_ids as $id) {
      $lista.=$id.",";
    }
    $lista = "(".substr($lista,0,-1).")";

    $query_rsLinguas = "SELECT sufixo FROM linguas WHERE visivel = '1'";
    $rsLinguas = DB::getInstance()->prepare($query_rsLinguas);
    $rsLinguas->execute();
    $row_rsLinguas = $rsLinguas->fetchAll();
    $totalRows_rsLinguas = $rsLinguas->rowCount();
  
    if($opcao == '-1') { // elimina utilizadores selecionados
      $query_rsProc = "SELECT imagem1, imagem2, imagem3, imagem4 FROM l_pecas_imagens WHERE id_peca IN $lista";
      $rsProc = DB::getInstance()->prepare($query_rsProc);
      $rsProc->execute();
      $totalRows_rsProc = $rsProc->rowCount();
      
      if($totalRows_rsProc > 0) {
        while($row_rsProc = $rsProc->fetch()) {
          @unlink('../../../imgs/produtos/'.$row_rsProc['imagem1']);
          @unlink('../../../imgs/produtos/'.$row_rsProc['imagem2']);
          @unlink('../../../imgs/produtos/'.$row_rsProc['imagem3']);
          @unlink('../../../imgs/produtos/'.$row_rsProc['imagem4']);
        }
      }
      
      $query_rsP = "DELETE FROM l_pecas_imagens WHERE id_peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();
      
      $query_rsP = "DELETE FROM l_pecas_categorias WHERE id_peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();
    
      $query_rsP = "DELETE FROM l_pecas_caract WHERE id_peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();
      
      $query_rsP = "DELETE FROM l_pecas_filtros WHERE id_peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();
    
      $query_rsP = "DELETE FROM l_pecas_desconto WHERE id_peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();

      $query_rsP = "DELETE FROM l_pecas_tamanhos WHERE peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();
    
      $query_rsP = "DELETE FROM l_pecas_relacao WHERE id_peca IN $lista";
      $rsP = DB::getInstance()->prepare($query_rsP);
      $rsP->execute();

      foreach($row_rsLinguas as $linguas) {
        $query_rsDel = "DELETE FROM l_pecas_".$linguas["sufixo"]." WHERE id IN $lista";
        $rsDel = DB::getInstance()->query($query_rsDel);
        $rsDel->execute();   
      }
    } 
    else if($opcao == 3 || $opcao == 4) { // colocar todas as not�cias vis�veis
      foreach($row_rsLinguas as $linguas) {
        if($opcao == 3) $query_rsUpd = "UPDATE l_pecas_".$linguas["sufixo"]." SET visivel = '1' WHERE id IN $lista";
        else $query_rsUpd = "UPDATE l_pecas_".$linguas["sufixo"]." SET visivel = '0' WHERE id IN $lista";
        $rsUpd = DB::getInstance()->query($query_rsUpd);
        $rsUpd->execute();
      }
    } 
    else if($opcao == 5 || $opcao == 6) { // destaques
      foreach($row_rsLinguas as $linguas) {
        if($opcao == 5) $query_rsUpd = "UPDATE l_pecas_".$linguas["sufixo"]." SET destaque = '1' WHERE id IN $lista";
        else $query_rsUpd = "UPDATE l_pecas_".$linguas["sufixo"]." SET destaque = '0' WHERE id IN $lista";
        $rsUpd = DB::getInstance()->query($query_rsUpd);
        $rsUpd->execute();
      }
    }
  }
  
  // ordena��o
  $sOrder = " ORDER BY id DESC";
  $colunas = array( '', 'ref', 'nome', 'promocao', 'ordem', 'destaque', 'visivel', '');
  if(isset($_REQUEST['order'])) {
    $sOrder = " ORDER BY ";
    $i=0;
    
    for($i=0; $i<sizeof($_REQUEST['order']); $i++) {
     if($i>0) $sOrder .= ", ";
     $sOrder .= $colunas[$_REQUEST['order'][$i]["column"]]." ".$_REQUEST['order'][$i]["dir"];
    }
  }
  
  // pesquisa
  $where_pesq = "";
  $left_join = "";
  if(isset($_REQUEST['action']) && $_REQUEST['action']=="filter") {
    $pesq_ref = $_REQUEST['form_ref'];
    $pesq_form= utf8_decode($_REQUEST['form_nome']);
    $pesq_destaque = $_REQUEST['form_destaque'];
    $pesq_visivel = $_REQUEST['form_visivel'];
    $pesq_categoria = $_REQUEST['form_categoria'];
    $pesq_marca = $_REQUEST['form_marca'];
    
    if($pesq_ref != "") $where_pesq .= " AND p.ref LIKE '%$pesq_ref%'";
    if($pesq_form != "") $where_pesq .= " AND p.nome LIKE '%$pesq_form%'";
    if($pesq_destaque != "") $where_pesq .= " AND p.destaque= '$pesq_destaque'";
    if($pesq_visivel != "") $where_pesq .= " AND p.visivel = '$pesq_visivel'";
    if($pesq_categoria != "") {
      if($pesq_categoria == 0) {
        if(CATEGORIAS == 1){
		      $where_pesq .= " AND (p.categoria = 0";
		    }
		    else if(CATEGORIAS == 2){
		      $where_pesq .= " AND (pc.id_categoria = 0";
		    }
      }
      else {
      	if(CATEGORIAS == 1){
		      $where_pesq .= " AND (p.categoria= '$pesq_categoria' OR c1.cat_mae = '$pesq_categoria'";
		    }
		    else if(CATEGORIAS == 2){
		      $where_pesq .= " AND (pc.id_categoria = '$pesq_categoria' OR c1.cat_mae = '$pesq_categoria'";
		    }

		    if(CATEGORIAS_NIVEL > 2){
			    for($i = 2; $i < CATEGORIAS_NIVEL; $i++) {		      
			      $where_pesq .= " OR c".$i.".cat_mae = '$pesq_categoria'";
			    }
			  }
      }
      $where_pesq .=')';
    }
    if($pesq_marca != "") {
      if($pesq_marca == 0) {
        $where_pesq .= " AND p.marca = 0";
      } 
      else {
        $where_pesq .= " AND p.marca = '$pesq_marca'";
      }
    }
  }

  if(CATEGORIAS_NIVEL > 2){
    for($i = 2; $i < CATEGORIAS_NIVEL; $i++) {
      $left_join .= " LEFT JOIN l_categorias_pt c".$i." ON c".$i.".id = c".($i-1).".cat_mae";
    }
  }
  
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  if(CATEGORIAS == 1){
  	$query_rsTotal = "SELECT p.*, m.nome as m_nome FROM l_pecas_pt p LEFT JOIN l_categorias_pt c1 ON c1.id = p.categoria ".$left_join." LEFT JOIN l_marcas_pt m ON p.marca = m.id WHERE p.id > '0'".$where_pesq." GROUP BY p.id ".$sOrder;
  }
  else if(CATEGORIAS == 2){
  	$query_rsTotal = "SELECT p.*, m.nome as m_nome FROM l_pecas_pt p LEFT JOIN l_pecas_categorias pc ON p.id = pc.id_peca LEFT JOIN l_categorias_pt c1 ON c1.id = pc.id_categoria ".$left_join." LEFT JOIN l_marcas_pt m ON p.marca = m.id WHERE p.id > '0'".$where_pesq." GROUP BY p.id ".$sOrder;
  }
  $rsTotal = DB::getInstance()->query($query_rsTotal);
  $rsTotal->execute();
  $totalRows_rsTotal = $rsTotal->rowCount();
  
  $iTotalRecords = $totalRows_rsTotal;
  
  if(CATEGORIAS == 1){
  	$query_rsTotal = "SELECT p.*, m.nome as m_nome FROM l_pecas_pt p LEFT JOIN l_categorias_pt c1 ON c1.id = p.categoria ".$left_join." LEFT JOIN l_marcas_pt m ON p.marca = m.id WHERE p.id > '0'".$where_pesq." GROUP BY p.id ".$sOrder." LIMIT $iDisplayStart, $iDisplayLength";
  }
  else if(CATEGORIAS == 2){
  	$query_rsTotal = "SELECT p.*, m.nome as m_nome FROM l_pecas_pt p LEFT JOIN l_pecas_categorias pc ON p.id = pc.id_peca LEFT JOIN l_categorias_pt c1 ON c1.id = pc.id_categoria ".$left_join." LEFT JOIN l_marcas_pt m ON p.marca = m.id WHERE p.id > '0'".$where_pesq." GROUP BY p.id ".$sOrder." LIMIT $iDisplayStart, $iDisplayLength";
  }
  $rsTotal = DB::getInstance()->query($query_rsTotal);
  $rsTotal->execute();
  $totalRows_rsTotal = $rsTotal->rowCount();
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
  
  $i = $iDisplayStart;
  while($i < $end && $row_rsTotal = $rsTotal->fetch()) {
    $id = $row_rsTotal['id'];
    $ref = $row_rsTotal['ref'];
    $nome = utf8_encode($row_rsTotal['nome']);
    $url = utf8_encode($row_rsTotal['url']);
    $marca = utf8_encode($row_rsTotal['m_nome']);
    
    //destaque
    if($row_rsTotal['destaque'] == 1) {
      $visivel_dest = $RecursosCons->RecursosCons['text_visivel_sim'];
      $etiqueta2_dest = "success";
    } 
    else {
      $visivel_dest = $RecursosCons->RecursosCons['text_visivel_nao'];
      $etiqueta2_dest = "danger";
    }

    //visivel
    if($row_rsTotal['visivel'] == 1) {
      $visivel =$RecursosCons->RecursosCons['text_visivel_sim'];
      $etiqueta2 = "success";
    } 
    else {
      $visivel = $RecursosCons->RecursosCons['text_visivel_nao'];
      $etiqueta2 = "danger";
    }
  
    
    $promo_val=""; if($row_rsTotal['promocao']>0) $promo_val=$row_rsTotal['promocao_desconto']; 
    
    $records["data"][] = array(
    '<input type="checkbox" id="check_'.$id.'" name="id[]" value="'.$id.'">',
    $ref,
    $nome,
    '<input type="text" id="discount_'.$id.'" name="discount_'.$id.'" class="cx_ordenar" value="'.$promo_val.'" onKeyPress="alteraOrdem(event)">',
    '<input type="text" id="order_'.$id.'" name="order_'.$id.'" class="cx_ordenar" value="'.$row_rsTotal['ordem'].'" onKeyPress="alteraOrdem(event)">',
    '<span class="label label-sm label-'.$etiqueta2_dest.'">'.utf8_encode($visivel_dest).'</span>',
    '<span class="label label-sm label-'.$etiqueta2.'">'.utf8_encode($visivel).'</span>',
    '<a href="produtos-edit.php?id='.$id.'" class="btn btn-xs default btn-editable"><i class="fa fa-pencil"></i>'.$RecursosCons->RecursosCons['btn_editar'].'</a>&nbsp;&nbsp;&nbsp;<a href="../../../'.$url.'" target="_blank" class="btn btn-xs default btn-editable" title="Preview" alt="Preview"><i class="fa fa-eye"></i></a>'
    );
    
    $i++;
  }

  DB::close();

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records);
?>
