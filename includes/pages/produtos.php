<?php include_once('pages_head.php');

if($_GET['id']) $categoria = $_GET['id'];
if($cat_redirect) $categoria = $cat_redirect;
if($sub_redirect) $catmae = $sub_redirect;
if($subsub_redirect) $submae = $subsub_redirect;

if(!$submae) {
  $submae = arraySearch($GLOBALS['divs_categorias'], 'id', $categoria, "cat_mae");
}
if(!$catmae) {
  $catmae = arraySearch($GLOBALS['divs_categorias'], 'id', $submae, "cat_mae");
}
if($catmae == $submae) {
  $submae = 0;
}

// echo "GET: ".$categoria."<br>";
// echo "redirect: ".$cat_redirect."<br>";
// echo $categoria."<br>";
// echo $catmae."<br>";
// echo $submae."<br>";

$extra_link = "";
$cat_refresh = 0;
$arrayCategorias = array();

if($categoria > 0) {
  array_push($arrayCategorias, $GLOBALS['divs_categorias'][$categoria]);

  if((int)$catmae > 0) {
      $arrayCategorias[0] = $GLOBALS['divs_categorias'][$catmae];
  }
}
else { //preciso para ter o link = "promocoes"
  $cat_refresh = 1;
  $arrayCategorias = $GLOBALS['divs_categorias'];

  if($meta_id == 4) { // NOVIDADES
    $categoria = "-1";
    $extra_link = "?filtrar_por=1";
  }
  if($meta_id == 5) { // PROMOCOES
    $categoria = "-2";
    $extra_link = "?filtrar_por=2";
  }
}

$query_rsPromoTexto = "SELECT texto_listagem FROM l_promocoes_textos".$extensao." WHERE id = '1'";
$rsPromoTexto = DB::getInstance()->prepare($query_rsPromoTexto);
$rsPromoTexto->execute();
$row_rsPromoTexto = $rsPromoTexto->fetch(PDO::FETCH_ASSOC);
$totalRows_rsPromoTexto = $rsPromoTexto->rowCount();
DB::close();

$menu_sel = "produtos";

?>
<main class="page-load produtos">
  <section class="div_100 categorias_banner" id="banners">

  </section>

  <nav class="breadcrumbs_cont" aria-label="You are here:" role="navigation">
    <div class="row">
      <div class="column">
        <ul class="breadcrumbs">
          <li class="disabled"><span><?php echo $Recursos->Resources["bread_tit"]; ?></span></li>
          <li><a href="<?php echo get_meta_link(1); ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/index.php" data-remote="false"><?php echo $Recursos->Resources["home"]; ?></a></li>
        </ul>
      </div>
    </div>
  </nav>

  <section class="container">
    <div class="row listings <?php echo $menu_sel; ?>_list">
      <div class="column">
        <div class="div_100">
          <div class="div_table_cell listings_filters">
            <div class="div_100 listings_filters_bg">
              <div class="div_100 listings_filters_content to_sticky" id="fixedFilters">
                <div class="filtersHead hide-for-medium">
                  <div class="row collapse align-middle">
                    <div class="column">
                      <button class="close-button invert relative" aria-label="Close filters" role="button" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="column shrink">
                      <h4 class="list_subtit icon-filtro"><?php echo $Recursos->Resources["mostrar_filtros"]; ?></h4>
                    </div>
                    <div class="column text-right">
                      <a class="clearFilters textos" href="javascript:;" onclick="window.location = location.protocol+'//'+location.host+location.pathname"><?php echo $Recursos->Resources["limpar_filtrar"]; ?></a>
                    </div>
                  </div>
                </div>
                <div class="div_100 filtersBody">
                  <div class="scroller">
                    <?php if(!empty($GLOBALS['divs_categorias'])) { ?>
                      <div id="filtersUrl" class="filters_divs" style="padding-top:0;">
                        <h3 class="subtitulos uppercase"><?php echo $Recursos->Resources["categorias"]; ?></h3>
                        <ul accordion accordion-icon="icon-down" accordion-clickToClose="false" accordion-allClosed="true">
                          <?php foreach($GLOBALS['divs_categorias'] as $cat) {
                            $subs = $cat['subs'];
                            if($cat['info']['nome']) {
                              $cat = $cat['info'];
                            }

                            $cats_el = " loja_inpt has-url";
                            if(empty($subs)) {
                              $cats_el = " loja_inpt has-url";
                            }
                            ?> 
                            <li accordion-item>
                              <a class="loja_inpt has-url type1 list_subtit<?php if($catmae == $cat['id'] || $categoria == $cat['id']) echo " active"; ?>" data-id="<?php echo $cat['id']; ?>" data-catmae="0" data-submae="0" data-url="<?php echo $cat['url'].$extra_link; ?>" href="<?php echo $cat['url'].$extra_link; ?>" accordion-title><?php echo $cat['nome']; ?></a>
                              <?php if(!empty($subs)) { ?>
                                <ul accordion-nested <?php if(($catmae == $cat['id'] || $categoria == $cat['id']) || count($GLOBALS['divs_categorias']) == 1) echo " content-visible"; ?>>
                                  <?php foreach($subs as $sub) {
                                    $subssubs = $sub['subs'];
                                    if($sub['info']['nome']) {
                                      $sub = $sub['info'];
                                    }
                                    ?> 
                                    <li accordion-item>
                                      <a class="filters loja_inpt has-url type2 list_txt <?php if($submae == $sub['id'] || $categoria == $sub['id']) echo " active"; ?>" href="javascript:;" accordion-title data-id="<?php echo $sub['id']; ?>" data-catmae="<?php echo $cat['id']; ?>" data-submae="0" name="sub" id="sub_<?php echo $sub['id']; ?>" data-url="<?php echo $sub['url']; ?>"><?php echo $sub['nome']; ?></a>
                                      <?php if(!empty($subssubs)) { ?>
                                        <ul accordion-nested <?php if($submae == $sub['id'] || $categoria == $sub['id']) echo " content-visible"; ?>>
                                          <?php foreach($subssubs as $subssub) { ?>
                                            <li accordion-item>
                                              <a class="filters" href="javascript:;" accordion-title>
                                                <input class="loja_inpt has-url type3 list_txt" type="checkbox" data-id="<?php echo $subssub['id']; ?>" data-catmae="<?php echo $cat['id']; ?>" data-submae="<?php echo $sub['id']; ?>" name="subssub" id="subssub_<?php echo $subssub['id']; ?>" data-url="<?php echo $subssub['url']; ?>" value="<?php echo $subssub['id']; ?>" <?php if($categoria==$subssub['id']) echo "checked"; ?> />
                                                <h5><?php echo $subssub["nome"]; ?></h5>
                                              </a>
                                            </li>
                                          <?php } ?>
                                        </ul>
                                      <?php } ?>
                                    </li>
                                  <?php } ?>
                                </ul>
                              <?php } ?>
                            </li>
                          <?php } ?>
                        </ul>
                      </div>
                    <?php } ?>
                    <input class="hidden" type="hidden" name="categoria" id="categoria" value="<?php echo $categoria; ?>"/>
                    <input class="hidden" type="hidden" name="catmae" id="catmae" value="<?php echo $catmae; ?>" />
                    <input class="hidden" type="hidden" name="submae" id="submae" value="<?php echo $submae; ?>" />

                    <div class="div_100" id="filtros_rpc">

                    </div>
                  </div>
                </div>  
                <div class="div_100 filtersFoot">
                  <a class="btnFilters button-big invert show-for-medium" href="javascript:;" onclick="window.location = location.protocol+'//'+location.host+location.pathname"><?php echo $Recursos->Resources["limpar_filtrar"]; ?></a>
                  <a class="btnFilters button-big invert hide-for-medium" href="javascript:;"><?php echo $Recursos->Resources["filtrar"]; ?></a>
                </div>
              </div>
            </div>
          </div>
          <div class="div_table_cell listings_container">
            <div class="listings_divs text-center">
              <div class="listing_mask">
                <div class="listing_loader to_sticky" id="listMask">
                  <div class="circle"></div>
                  <div class="line-mask">
                    <div class="line"></div>
                  </div>
                </div>
              </div>
              <?php if($row_rsPromoTexto['texto_listagem']) { ?>
                <div class="listagem_promocoes row collapse text-center">
                  <div class="column">
                    <div class="listagem_promocoes_txt">
                      <?php echo $row_rsPromoTexto['texto_listagem']; ?>
                    </div>
                  </div>
                </div>
              <?php } ?>
              <div class="row collapse small-up-1 xsmall-up-2 xxsmall-up-3 text-center" id="<?php echo $menu_sel; ?>">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>    

  <div id="sticked_filter" class="div_100 sticked_filter to_sticky hide-for-medium text-center">
    <button class="filtersToggle text-center" type="button">
      <span class="list_subtit icon-filter"><?php echo $Recursos->Resources["mostrar_filtros"]; ?></span>
    </button>
  </div>
</main>
<?php include_once('pages_footer.php'); ?>