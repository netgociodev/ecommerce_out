<?php include_once('pages_head.php');

if($cat_redirect) {
  $categoria = $cat_redirect;
}
if($sub_redirect) {
  $catmae = $sub_redirect;
}
if($subsub_redirect) {
  $submae = $subsub_redirect;
}

if(!$submae) {
  $submae = arraySearch($GLOBALS['divs_categorias'], 'id', $categoria, "cat_mae");
}
if(!$catmae) {
  $catmae = arraySearch($GLOBALS['divs_categorias'], 'id', $submae, "cat_mae");
}
if($catmae == $submae) {
  $submae = 0;
}

if($produto_redirect) {
  $produto = $produto_redirect;
}

if($_GET['id']) {
  $produto = $_GET['id'];
}

$id_cliente = $row_rsCliente['id'];

$query_rsProduto = "SELECT * FROM l_pecas".$extensao." WHERE id = :id";
$rsProduto = DB::getInstance()->prepare($query_rsProduto);
$rsProduto->bindParam(':id', $produto, PDO::PARAM_INT); 
$rsProduto->execute();
$row_rsProduto = $rsProduto->fetch(PDO::FETCH_ASSOC);
$totalRows_rsProduto = $rsProduto->rowCount();

$query_rsImgs = "SELECT * FROM l_pecas_imagens WHERE id_peca = :id AND visivel = 1 ORDER BY ordem ASC, id DESC";
$rsImgs = DB::getInstance()->prepare($query_rsImgs);
$rsImgs->bindParam(':id', $produto, PDO::PARAM_INT); 
$rsImgs->execute();
$row_rsImgs = $rsImgs->fetchAll();
$totalRows_rsImgs = $rsImgs->rowCount();

$row_rsCatMae = array();
$row_rsSubMae = array();
$row_rsCategoria = array();

if($catmae > 0 && $submae == 0) {
  $row_rsCatMae = $GLOBALS['divs_categorias'][$catmae]['info'];
  $row_rsCategoria = $GLOBALS['divs_categorias'][$catmae]['subs'][$categoria]['info'];
}
else if($catmae > 0 && $submae > 0) {    
  $row_rsCatMae = $GLOBALS['divs_categorias'][$catmae]['info'];
  $row_rsSubMae = $GLOBALS['divs_categorias'][$catmae]['subs'][$submae]['info'];
  $row_rsCategoria = $GLOBALS['divs_categorias'][$catmae]['subs'][$submae]['subs'][$categoria];
}

if(tableExists(DB::getInstance(), 'l_marcas_pt') && ECC_MARCAS == 1 && $row_rsProduto['marca'] > 0) {
  $query_rsMarca = "SELECT * FROM l_marcas".$extensao." WHERE id = '$row_rsProduto[marca]'";
  $rsMarca = DB::getInstance()->query($query_rsMarca);
  $row_rsMarca = $rsMarca->fetch(PDO::FETCH_ASSOC);
  $totalRows_rsMarca = $rsMarca->rowCount();
}

$query_rsRelacionados = "SELECT pecas.* FROM l_pecas_relacao AS relacao, l_pecas".$extensao." AS pecas WHERE pecas.id = relacao.id_relacao AND relacao.id_peca = :id AND pecas.visivel='1' ORDER BY rand() LIMIT 25";
$rsRelacionados = DB::getInstance()->prepare($query_rsRelacionados);
$rsRelacionados->bindParam(':id', $produto, PDO::PARAM_INT, 5); 
$rsRelacionados->execute();
$row_rsRelacionados = $rsRelacionados->fetchAll();
$totalRows_rsRelacionados = $rsRelacionados->rowCount();

//Carrega produtos associados �s mesmas �reas se n�o tiver relacionados
if($totalRows_rsRelacionados == 0) { 
  if(CATEGORIAS == 2) {
    $query_rsRelacionados = "SELECT pecas.* FROM l_pecas".$extensao." AS pecas LEFT JOIN l_pecas_categorias AS pecas_cat ON pecas.id = pecas_cat.id_peca WHERE pecas_cat.id_categoria = '$categoria' AND pecas.id != '$produto' AND pecas.visivel='1' ORDER BY rand() LIMIT 25";
  }
  else {
    $query_rsRelacionados = "SELECT pecas.* FROM l_pecas".$extensao." AS pecas WHERE pecas.categoria = '$categoria' AND pecas.id != '$produto' AND pecas.visivel='1' ORDER BY rand() LIMIT 25";
  }
  $rsRelacionados = DB::getInstance()->query($query_rsRelacionados);
  $row_rsRelacionados = $rsRelacionados->fetchAll();
  $totalRows_rsRelacionados = $rsRelacionados->rowCount();   
}

if(CATEGORIAS == 1) {
  $query_rsTotal = "SELECT peca.id, peca.url, peca.imagem2, peca.nome FROM l_pecas".$extensao." AS peca WHERE peca.visivel = '1' AND peca.categoria = '$categoria' GROUP BY peca.id ORDER BY peca.ordem ASC, peca.id DESC";
  $rsTotal = DB::getInstance()->prepare($query_rsTotal);
  $rsTotal->execute();
  $totalRows_rsTotal = $rsTotal->rowCount();
}
else if(CATEGORIAS == 2) {
  $query_rsTotal = "SELECT peca.id, peca.url, peca.imagem2, peca.nome FROM l_pecas".$extensao." AS peca LEFT JOIN l_pecas_categorias AS pca_cat ON pca_cat.id_peca = peca.id LEFT JOIN l_categorias".$extensao." AS cat1 ON cat1.id = pca_cat.id_categoria LEFT JOIN l_categorias".$extensao." AS cat2 ON cat2.id = cat1.cat_mae WHERE peca.visivel = '1' AND (cat1.id = :categoria OR cat2.id = :categoria OR cat2.cat_mae = :categoria) GROUP BY peca.id ORDER BY peca.ordem ASC, peca.id DESC";
  $rsTotal = DB::getInstance()->prepare($query_rsTotal);
  $rsTotal->bindParam(':categoria', $categoria, PDO::PARAM_INT, 5); 
  $rsTotal->execute();
  $totalRows_rsTotal = $rsTotal->rowCount();
}

$prod_ant = "";
$prod_ant_img = "";
$prod_ant_nome = "";
$prod_seg = "";
$prod_seg_img = "";
$prod_seg_nome = "";
$encontrado = 0;
$conta_reg = 0;

if($totalRows_rsTotal > 1) {
  while($row_rsTotal = $rsTotal->fetch()) { 
    $registo_actual++;
    
    if($encontrado == 1) {
      $prod_seg = $row_rsTotal['url'];
      $prod_seg_id = $row_rsTotal['id'];
      $prod_seg_nome = $row_rsTotal['nome'];
      break;          
    }
    
    if($row_rsTotal['id'] != $produto && $encontrado == 0) {
      $prod_ant = $row_rsTotal['url'];
      $prod_ant_id = $row_rsTotal['id'];
      $prod_ant_nome = $row_rsTotal['nome'];
    } 
    else if($row_rsTotal['id'] == $produto) {
      $encontrado = 1;
    }
  }
}
else {
  $registo_actual++;
}


//Array com os detalhes da promocao do produto (se tiver)
//Pos. 0: Datas
//Pos. 1: T�tulo
//Pos. 2: Texto
//Pos. 3: P�gina
$array_promocao = $class_produtos->promocaoProduto($row_rsProduto['id']);

$meta_og = 1;
$share_img = ROOTPATH_HTTP."imgs/produtos/".$row_rsProduto['imagem1'];

$menu_sel = "produtos";
$menu_sel2 = "detalhe";
?>
<main class="div_100 product-detail detalhe_container">
  <nav class="breadcrumbs_cont" aria-label="You are here:" role="navigation">
    <div class="row">
      <div class="column">
        <ul class="breadcrumbs">
          <li class="disabled"><span><?php echo $Recursos->Resources["bread_tit_det"]; ?></span></li>
          <li><a href="<?php echo get_meta_link(1); ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/index.php" data-remote="false"><?php echo $Recursos->Resources["home"]; ?></a></li>
          <li><a href="loja" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/produtos.php" data-ajaxTax="0" data-remote="false"><?php echo $Recursos->Resources['produtos']; ?></a></li>
          <?php if(!empty($row_rsCatMae)) { ?>
            <li><a href="<?php echo $row_rsCatMae["url"]; ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/produtos.php" data-ajaxTax="<?php echo $row_rsCatMae['id']; ?>" data-remote="false"><?php echo $row_rsCatMae["nome"]; ?></a></li>
          <?php } ?>
          <?php if(!empty($row_rsSubMae)) { ?>
            <li><a href="<?php echo $row_rsSubMae["url"]; ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/produtos.php" data-ajaxTax="<?php echo $row_rsSubMae['id']; ?>" data-remote="false"><?php echo $row_rsSubMae["nome"]; ?></a></li>
          <?php } ?>
          <li><a href="<?php echo $row_rsCategoria["url"]; ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/produtos.php" data-ajaxTax="<?php echo $row_rsCategoria['id']; ?>" data-remote="false"><?php echo $row_rsCategoria["nome"]; ?></a></li>
          <li class="show-for-medium"> <span><?php echo $row_rsProduto["nome"]; ?></span></li>
        </ul>
      </div>
      <div class="column shrink">
        <nav class="detalhe_nav">
          <?php if($prod_ant) { ?>
            <a class="textos" href="<?php echo $prod_ant; ?>">
              <i class="square icon-left"></i><!-- 
              --><span><?php echo $Recursos->Resources["anterior"]; ?></span>
            </a>
          <?php } ?>
          <?php if($prod_seg) { ?>
            <a class="textos" href="<?php echo $prod_seg; ?>">
              <span><?php echo $Recursos->Resources["seguinte"]; ?></span><!-- 
              --><i class="square icon-right"></i>    
            </a>
          <?php } ?>
        </nav>
      </div>
    </div>
  </nav>

  <div class="div_100 info_container">
    <div class="row align-center">
      <div class="small-12 xsmall-9 xxsmall-7 medium-3 xmedium-4 column"> 
        <div class="div_100 detalhe_info hide-for-xxsmall">
          <h1><?php echo $row_rsProduto['nome']; ?></h1>
          <h3>Ref. <?php echo $row_rsProduto['ref']; ?></h3>
          <?php if($row_rsMarca['nome']) { ?>
            <h3><strong><?php echo $Recursos->Resources['marca']; ?></strong> <?php echo $row_rsMarca['nome']; ?></h3>
          <?php } ?>
        </div>
        <div id="div_imagem" class="div_100 product-detail-img">
          <?php echo $class_produtos->labelsProduto($row_rsProduto['id'], 1, 'detalhe'); ?>
          <?php if($totalRows_rsImgs > 0) { ?>
            <button class="arrows_slick produtos_arrows show_arrows prev hide-for-medium" aria-label="Prev Arrow" role="button">
              <span class="icon-left"></span>
            </button>       
            <button class="arrows_slick produtos_arrows show_arrows next hide-for-medium" aria-label="Next Arrow" role="button">
              <span class="icon-right"></span>
            </button>  
            <div class="slick-imgs" style="display:block;">
              <?php foreach($row_rsImgs as $imgs) {
                $img_gr =  $img = ROOTPATH_HTTP."imgs/elem/geral.jpg"; 
                if($imgs['imagem1'] && file_exists(ROOTPATH."imgs/produtos/".$imgs['imagem1'])) {
                  $img_gr = ROOTPATH_HTTP."imgs/produtos/".$imgs['imagem1']; 
                }
                if($imgs['imagem2'] && file_exists(ROOTPATH."imgs/produtos/".$imgs['imagem2'])) {
                  $img = ROOTPATH_HTTP."imgs/produtos/".$imgs['imagem2']; 
                }
                ?>
                <a data-id="<?php echo $imgs['id']; ?>" data-tamanho="<?php echo $imgs['id_tamanho']; ?>" href="<?php echo $img_gr; ?>" class="item has_bg contain" style="background-image:url('<?php echo $img; ?>');">
                  <?php echo getFill('produtos', 2); ?>
                </a>
              <?php } ?> 
            </div>
          <?php } else { 
            $img_gr = $img = ROOTPATH_HTTP."imgs/elem/geral.jpg"; 
            ?>
            <a class="div_100 item has_bg contain slick-current" href="<?php echo $img_gr; ?>"  style="background-image:url('<?php echo $img; ?>');">
              <?php echo getFill('produtos', 2); ?>
            </a>
          <?php } ?>
          <?php if($totalRows_rsImgs > 0) { ?>
            <div class="icon-zoom show-for-medium" onclick="$('.item:eq(0)').click();"></div>
          <?php } ?>
        </div>
        <?php if($totalRows_rsImgs > 1) { ?>
          <div class="div_100 detalhe_thumbs show-for-medium">
            <div class="slick-thumbs" style="display:block;">
              <?php foreach($row_rsImgs as $imgs) {
                $img = ROOTPATH_HTTP."imgs/elem/geral.jpg"; 
                if($imgs['imagem2'] && file_exists(ROOTPATH."imgs/produtos/".$imgs['imagem4'])) {
                  $img = ROOTPATH_HTTP."imgs/produtos/".$imgs['imagem4']; 
                }
                ?><!--
                --><div class="thumbs has_bg contain" style="background-image:url('<?php echo $img; ?>');">
                  <?php echo getFill('produtos', 4); ?>
                </div><!--
              --><?php } ?>
            </div>
          </div>
        <?php } ?>
      </div>     
      <?php $detalhe = 1; include_once(ROOTPATH.'includes/produtos-info.php'); ?>                         
    </div> 
  </div>

  <?php if($totalRows_rsCombina > 0) { ?>
    <div class="div_100 relacionados_cont" id="combina">
      <div class="row">
        <div class="column">
          <div class="titles"><h3 class="titulos"><?php echo $Recursos->Resources['prods_combina']; ?></h3></div>
          <div class="listagem">
            <div class="slick-combina has_cursor">
            <?php foreach($row_rsCombina as $combina) { 
              echo $class_produtos->divsProduto($combina, '');
            } ?> 
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
    
  <?php if($totalRows_rsRelacionados > 0) { ?>
    <div class="div_100 relacionados_cont" id="relacionados">
      <div class="row">
        <div class="column">
          <div class="titles"><h3 class="titulos"><?php echo $Recursos->Resources['prods_relacionados']; ?></h3></div>
          <div class="listagem">
            <div class="slick-relacionados has_cursor">
            <?php foreach($row_rsRelacionados as $relacionados) { 
              echo $class_produtos->divsProduto($relacionados, '');
            } ?> 
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</main>
<?php 
DB::close();
include_once('pages_footer.php'); 
?>