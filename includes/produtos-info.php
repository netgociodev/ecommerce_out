<?php 
if($row_rsCliente != 0) {
  $id_cliente = $row_rsCliente['id'];
  $where_list = "lista.cliente = '$id_cliente'";
}
else {
  $wish_session = $_COOKIE[WISHLIST_SESSION];
  
  if($wish_session) {
    $where_list = "lista.session = '$wish_session'";
  }
}

$totalRows_rsFavorito = 0;
if($where_list) {
  $query_rsFavorito = "SELECT lista.id FROM lista_desejo AS lista LEFT JOIN l_pecas".$extensao." AS pecas ON lista.produto = pecas.id WHERE ".$where_list." AND lista.produto = '".$produto."' AND pecas.visivel = 1 GROUP BY pecas.id ORDER BY pecas.ordem ASC";
  $rsFavorito = DB::getInstance()->prepare($query_rsFavorito);
  $rsFavorito->execute();
  $row_rsFavorito = $rsFavorito->fetch(PDO::FETCH_ASSOC);
  $totalRows_rsFavorito = $rsFavorito->rowCount();
}
?>
<div class="column small-12<?php if($menu_sel == "produtos") echo " xxsmall-6 medium-5"; ?>">
  <div class="div_100 product-detail-info"<?php if($menu_sel != "produtos") echo ' style="padding: 0; margin-bottom: 2rem;"'; ?>>
    <div class="div_100 show-for-xxsmall">
      <h1><?php echo $row_rsProduto['nome']; ?></h1>
      <?php if($row_rsProduto['ref']) { ?>
        <h3>Ref. <?php echo $row_rsProduto['ref']; ?></h3>
      <?php } ?>
      <?php if($row_rsMarca['nome']) { ?>
        <h3><strong><?php echo $Recursos->Resources['marca']; ?></strong> <?php echo $row_rsMarca['nome']; ?></h3>
      <?php } ?>
    </div>  
    <div class="detalhe_divs">
      <?php if($row_rsProduto['descricao']) { ?>
        <ul accordion styled id="desc_accordion">
          <li accordion-item>
            <a href="javascript:;" class="list_tit" accordion-title><?php echo $Recursos->Resources['descricao']; ?></a>
            <div class="textos" accordion-content><?php echo $row_rsProduto['descricao']; ?></div>
          </li>
        </ul>
      <?php } ?>
      <div class="div_100 share_section text-left" id="share">
        <?php
        $sharePos = "bottom";
        $shareClass = "shareInvert"; //shareInvert
        $shareTitulo = $Recursos->Resources["partilhar"];
        $shareNome = urlencode(utf8_encode($row_rsProduto["nome"]));
        $shareDesc = urlencode(str_replace(utf8_encode('"'), "'", $row_rsProduto["descricao"]));
        $shareUrl = ROOTPATH_HTTP.$row_rsProduto["url"];
        if($countLang > 1) {
          $shareUrl = ROOTPATH_HTTP.$lang."/".$row_rsProduto["url"];
        }
        $shareImg = ROOTPATH_HTTP."/imgs/produtos/".$row_rsProduto["imagem1"];
        include_once(ROOTPATH.'includes/share-list.php');
        ?>
      </div>
      <?php if(!empty($array_promocao) && $detalhe == 1) { ?>
        <div class="div_100 detalhe_promocoes row collapse">
          <div class="column">
            <div class="row collapse">
              <div class="column detalhe_promocoes_tit">
                <?php if($array_promocao['1'] != '') echo $array_promocao['1']; else echo $Recursos->Resources['promocao_titulo']; ?>
              </div>
            </div>
            <?php if($array_promocao['0'] != '') { ?>
              <div class="row collapse">
                <div class="column detalhe_promocoes_datas">
                  <?php echo $array_promocao['0']; ?>
                </div>
              </div>
            <?php }
            if($array_promocao['2'] != '') { ?>
              <div class="row collapse">
                <div class="column detalhe_promocoes_txt">
                  <?php echo $array_promocao['2']; ?>
                </div>
              </div>
            <?php }
            if($array_promocao['3'] != '') { 
              $query_rsPaginaCondicoes = "SELECT url FROM paginas".$extensao." WHERE id = '".$array_promocao['3']."' AND visivel = 1";
              $rsPaginaCondicoes = DB::getInstance()->prepare($query_rsPaginaCondicoes);
              $rsPaginaCondicoes->execute();
              $row_rsPaginaCondicoes = $rsPaginaCondicoes->fetch(PDO::FETCH_ASSOC);
              $totalRows_rsPaginaCondicoes = $rsPaginaCondicoes->rowCount();

              if($totalRows_rsPaginaCondicoes > 0) { ?>
                <div class="row collapse">
                  <div class="column detalhe_promocoes_pagina">
                    <a href="<?php echo ROOTPATH_HTTP.$row_rsPaginaCondicoes['url']; ?>" target="_blank" class="button invert2"><?php echo $Recursos->Resources["promocao_link"]; ?></a>
                  </div>
                </div>
              <?php }
            } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<div class="column small-12<?php if($menu_sel == "produtos") echo " xxsmall-6 medium-4 xmedium-3"; ?>">
  <div id="prod_opc_1_<?php echo $produto; ?>">
    <?php echo $class_produtos->tamanhosProduto($row_rsProduto["id"]); ?>
  </div>
  <div id="prod_opc_2_<?php echo $produto; ?>" style="display: none;"></div>
  <div id="prod_opc_3_<?php echo $produto; ?>" style="display: none;"></div>
  <div id="prod_opc_4_<?php echo $produto; ?>" style="display: none;"></div>
  <div id="prod_opc_5_<?php echo $produto; ?>" style="display: none;"></div>
    
  <div class="tamanhos_divs">
    <label class="list_tit" for="quantidades"><?php echo $Recursos->Resources["cart_qtd"]; ?></label><!--
    --><div class="select_holder icon-down">
      <select name="quantidades" id="qtd_<?php echo $produto; ?>" required data-produto="<?php echo $produto; ?>">
        <?php for($i = 1; $i <= 10; $i++) { ?>
          <option value="<?php echo $i; ?>" <?php if($i == $quantidade) echo "selected"; ?>><?php echo $i; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="detalhe_quantidade">
    <div class="detalhe_preco" id="conteudo_preco_<?php echo $row_rsProduto['id']; ?>">
      <?php echo $class_produtos->precoProduto($row_rsProduto['id']); ?><!-- 
      --><?php echo $class_produtos->labelsProduto($row_rsProduto['id'], 2, 'detalhe'); ?>
      <input name="preco_final" id="preco_final_<?php echo $row_rsProduto['id']; ?>" type="hidden" value="<?php echo $class_produtos->precoProduto($row_rsProduto['id'], 0); ?>" />
    </div>

    <?php if(!empty($array_promocao) && $detalhe != 1) { ?>
      <div class="div_100 detalhe_divs_promocoes row collapse">
        <div class="column detalhe_promocoes_datas">
          <?php echo $array_promocao['0']; ?>
          <a href="<?php echo $row_rsProduto['url']; ?>" class="detalhe_promocoes_link"><?php echo $Recursos->Resources['promocoes_ver_mais']; ?></a>
        </div>
      </div>
    <?php } ?>

    <a href="javascript:;" class="detalhe_adiciona adiciona_carrinho button-big invert2" data-product="<?php echo $row_rsProduto['id']; ?>"><?php echo $Recursos->Resources["add_carrinho2"]; ?></a>
    <a href="javascript:;" class="favoritos text-center<?php if($totalRows_rsFavorito > 0) echo " icon-favoritos2 active"; else echo " icon-favoritos"; ?>" onClick="adiciona_favoritos(<?php echo $row_rsProduto['id']; ?>, this, event);"><?php echo $Recursos->Resources["add_favoritos"]; ?></a>
      
    <div class="stock list_txt" id="conteudo_stock"><?php echo $class_produtos->stockProduto($row_rsProduto['id'], 0, 0, 0, 0, 0, 3); ?></div>
  </div>
</div>  