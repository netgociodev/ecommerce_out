<?php require_once('Connections/connADMIN.php'); ?>
<?php

if(!$query_rsMeta) $query_rsMeta = "SELECT * FROM metatags".$extensao." WHERE id = :id";
$rsMeta = DB::getInstance()->prepare($query_rsMeta);
$rsMeta->bindParam(':id', $meta_id, PDO::PARAM_INT, 5); 
$rsMeta->execute();
$row_rsMeta = $rsMeta->fetch(PDO::FETCH_ASSOC);
$totalRows_rsMeta = $rsMeta->rowCount();

$title = $row_rsMeta["title"];
$description = $row_rsMeta["description"];
$keywords = $row_rsMeta["keywords"];

$query_rsPopUp = "SELECT * FROM config WHERE id='1'";
$rsPopUp = DB::getInstance()->prepare($query_rsPopUp);  
$rsPopUp->execute();
$row_rsPopUp = $rsPopUp->fetch(PDO::FETCH_ASSOC);
$totalRows_rsPopUp = $rsPopUp->rowCount();
$popup = 1;
if(!$row_rsPopUp['ativo']) {
  $popup = 0;
}
else {
  if(!$row_rsPopUp['imagem_popup'] || !file_exists(ROOTPATH.'imgs/popup/'.$row_rsPopUp['imagem_popup'])) {
    $popup = 0;
  }

  if($row_rsPopUp['tipo_popup'] == 1 && $_SESSION['popup_closed'] == 1) {
    $popup = 0;
  }
}

$ishome = 1;
if(!$changeMetas) {
  $changeMetas = 0;
}

$loader = file_get_contents(ROOTPATH.'imgs/elem/loader.svg');

$no_scripts = 1;

$footer_small = 0;

$mostra_entrada = 0;
if(!isset($_SESSION['mostra_entrada'])) {
  $_SESSION['mostra_entrada'] = 1;
  $mostra_entrada = 1;
}

DB::close();

?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>"><head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame - Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>
<?php if($title){ echo htmlspecialchars($title, ENT_COMPAT, 'ISO-8859-1'); }else{ echo $Recursos->Resources["pag_title"];}?>
</title>
<?php if($description){?>
<META NAME="description" CONTENT="<?php echo addcslashes(htmlspecialchars($description, ENT_COMPAT, 'ISO-8859-1'), '"\\/'); ?>" />
<?php }?>
<?php if($keywords!=""){?>
<META NAME="keywords" CONTENT="<?php echo addcslashes(htmlspecialchars($keywords, ENT_COMPAT, 'ISO-8859-1'), '"\\/'); ?>" />
<?php }?>
<?php include_once('codigo_antes_head.php'); ?>
</head>
<body<?php if($mostra_entrada == 1) echo ' class=" mask-visible"'?>>
<!--Preloader-->
<div class="mask">
  <div id="loader">
    <div class="div_100 svg_load">
      <?php echo $loader; ?>
    </div>
  </div>
</div>

<?php if($file_to_include && $file_to_include!='404.php' && file_exists(ROOTPATH.'includes/pages/'.$file_to_include)) { ?>
<div class="mainDiv"> 
  <section class="row1">
    <div class="div_table_cell">
      <?php include_once('header.php'); ?>
      <div class="page-main page-current">
        <?php if($file_to_include && file_exists(ROOTPATH.'includes/pages/'.$file_to_include)) {
          include_once(ROOTPATH."includes/pages/".$file_to_include);
        }
        else {
          include_once(ROOTPATH."includes/pages/404.php");
        } ?>
      </div>
      <div class="page-main page-next" aria-hidden="true"></div>
      <div class="page-main page-prev" aria-hidden="true"></div>
    </div>
  </section>
  <section class="row2">
    <div class="div_table_cell">
      <?php include_once('footer.php'); ?>
    </div>
  </section>
</div>
<?php } else {
  include_once(ROOTPATH."includes/pages/404.php");
} ?>

<div id="details-modal" class="horizontal is-shape-bg">
  <div class="content_inner"></div><!-- 
   --><div class="shape-wrap">
    <div class="shape-bg">
      <div class="div_100" style="height: 100%">
        <div class="div_table_cell">
          <?php echo $loader; ?>
        </div>
      </div>
    </div>
  </div><!-- 
  --><div id="produtos-detail-ajax" class="detail-ajax"<?php if($url_open) echo 'data-willopen="'.$url_open.'"';?>></div><!-- 
  --><div id="noticias-detail-ajax" class="detail-ajax"></div>
</div>

<div class="loading-transition">
  <div class="div_100 loading-animation">
    <?php echo $loader; ?>
  </div>
</div>

<?php include_once('footer_scripts.php'); ?>

<?php if($changeMetas == 1) { ?>
  <script type="text/javascript">
    $(window).on('load', function() {
      handleMetas('', '');
    });
  </script>
<?php }?>

<?php if(isset($_GET['subs']) && $_GET['subs'] == 1) { ?>
  <script type="text/javascript">
    $(window).on('load', function(){
      ntg_success("<?php echo $Recursos->Resources["mail_msg_3"]; ?>");
      window.history.replaceState('Object', document.title, 'index.php');
    });
  </script>
<?php } ?>

<?php include_once('codigo_antes_body.php'); ?>
</body>
</html>