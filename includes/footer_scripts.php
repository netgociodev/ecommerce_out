<script src="<?php echo ROOTPATH_HTTP; ?>js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAi4pzXIaemolBXDPx_xYjp-97HMC209GI"></script>
<script type="text/javascript" src='<?php echo ROOTPATH_HTTP; ?>js/netgocio.main.min.js'></script>

<?php if(CAPTCHA_KEY != NULL) { ?>
  <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>" async defer></script>
<?php } ?>

<script type="text/javascript" src='<?php echo ROOTPATH_HTTP; ?>js/workers.main.js'></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
<script>
 WebFont.load({
 	<?php echo FONT_SITE; ?>
  });
</script>

<?php if($_GET['env'] > 0) {
  $query_rsNotificacao = "SELECT sucesso FROM notificacoes".$extensao." WHERE id=:id";
  $rsNotificacao = DB::getInstance()->prepare($query_rsNotificacao);
  $rsNotificacao->bindParam(':id', $_GET['env'], PDO::PARAM_INT);      
  $rsNotificacao->execute();
  $row_rsNotificacao = $rsNotificacao->fetch(PDO::FETCH_ASSOC);
  $totalRows_rsNotificacao = $rsNotificacao->rowCount();
  DB::close();
  
  $texto_env = $Recursos->Resources["formulario_contacto_msg"];
  if($row_rsNotificacao['sucesso']) {
    $texto_env = $row_rsNotificacao['sucesso'];
  }

  $msg_alertify = nl2br($texto_env);
  $msg_alertify = str_replace(array("\r\n", "\n"), "", $msg_alertify);
  ?>
  <script type="text/javascript">
    $(window).on('load', function() {
      ntg_alert("<?php echo $msg_alertify; ?>");
    });
  </script>
<?php } ?>