<?php 
if(!$sharePos) $sharePos = "top";
?>
<div class="share_container">
  <?php if($shareSize != "small") { ?>
    <div class="share_list shareInvert">
      <?php if($shareTitulo){?><span><?php echo $shareTitulo; ?></span><?php }?><!-- 
      --><div class="share-button share-facebook-circle" data-link="http://www.facebook.com/sharer.php?u=<?php echo $shareUrl; ?>"></div><!--
      --><div class="share-button share-linkedin-circle" data-link="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $shareUrl; ?>&title=<?php echo $shareNome; ?>&summary=<?php echo $shareDesc; ?>&source=<?php echo NOME_SITE; ?>"></div><!--
      --><a class="share-button share-mail-circle" href="mailto:?subject=<?php echo NOME_SITE; ?> - <?php echo $shareNome; ?>&body=<?php echo $shareUrl; ?>"></a>  
      <div class="share-button share-share-circle share-opener"></div>
    </div>
  <?php } else { ?>
    <div class="share_list">
      <div class="share-button share-share share-opener"></div>
    </div>
  <?php } ?>
  <div class="share_modal <?php echo $shareClass; ?> <?php echo $sharePos; ?> text-left">
    <div class="share-button share-facebook-circle" data-link="http://www.facebook.com/sharer.php?u=<?php echo $shareUrl; ?>">Facebook</div>
    <div class="share-button share-linkedin-circle" data-link="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $shareUrl; ?>&title=<?php echo $shareNome; ?>&summary=<?php echo $shareDesc; ?>&source=<?php echo NOME_SITE; ?>">LinkedIn</div>
    <div class="share-button share-twitter-circle" data-link="http://twitter.com/share?text=<?php echo NOME_SITE; ?> - <?php echo $shareNome; ?>&url=<?php echo $shareUrl; ?>">Twitter</div>
    <div class="share-button share-pinterest-circle" data-link="//pinterest.com/pin/create/link/?url=<?php echo $shareUrl; ?>&media=<?php echo $shareImg; ?>&description=<?php echo $shareDesc; ?>">Pinterest</div>
    <div class="share-button share-messenger-circle" data-link="http://www.facebook.com/dialog/send?app_id=182447472173773&amp;link=<?php echo $shareUrl; ?>&amp;redirect_uri=<?php echo $shareUrl; ?>">Messenger</div>
    <a class="share-button share-whatsapp-circle" href="whatsapp://send?text=<?php echo $shareDesc; ?>" data-action="share/whatsapp/share">WhatsApp</a>
    <a class="share-button share-mail-circle" href="mailto:?subject=<?php echo NOME_SITE; ?> - <?php echo $shareNome; ?>&body=<?php echo $shareUrl; ?>">Email</a>
  </div>
</div>